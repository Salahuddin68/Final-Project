<?php include 'header.php';  ?>

    <body class="panel-access">

        <div id="layout">
             <!--Login-->
                <div class="login">
                    <div class="container">
                        <div class="login-form">
                            <div class="data-form">
                                <span class="back-to-homepage">
                                    <a class="btn btn-green btn-xsmall" href="index.php"><i class="fa fa-home"></i> Back to Homepage</a>
                                </span>
                                <!--Logo-->
                                <a href="login.php" class="logo reg-logo"><img src="images/login-logo.png" alt="logo"></a>
                                <!--Logo-->

                                <!--Form-->
                                <form class="form-login reset">
                                    <div class="alert alert-warning" role="alert">Please, enter your email and then check your inbox to reset the pass.</div>

                                    <div class="icon-data">
                                        <i class="fa fa-envelope-open-o"></i>
                                    </div>

                                    <input type="text" placeholder="Email">

                                    <button class="btn btn-red" type="submit">Reset</button>
                                </form>
                                <!--Form-->

                                <span class="help">
                                    <a href="login.php"><i class="fa fa-angle-double-left"></i> Back to Login </a>
                                    <a href="help.php" class="help-link">Help?</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php include 'footer.php';  ?>

