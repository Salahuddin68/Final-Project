<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `reports`";
// var_dump($query);
include 'header.php'; 
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid margin-top">
            <a href="Add-report.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Report</a>  
            <div class="table-responsive">     
            <table class="table table-hover margin-top">
              <thead>
                <tr>
                  <th>Report Id</th>
                  <th>Patient Id</th>
                  <th>Patient Name</th>
                  <th>Report Title</th>
                  <th>Report Picture</th>
                  <th>Created At</th>
                  <th>Modified At</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                  <!-- <tr>
                    <td>1</td>
                    <td>Jalal</td>
                    <td>Blood Test</td>
                    <td>picture</td>
                    <td>
                      <a href="#" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="#" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="#" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr> -->
            
                   <?php 
                  foreach($db->query($query) as $reports) { ?>
                  <tr>
                    <td><?php echo $reports['id'];?></td>
                    <td><?php echo $reports['patients_id'];?></td>
                    <td><?php echo $reports['patients_name'];?></td>
                    <td><?php echo $reports['report_title'];?></td>
                    <td><img width="80px" src="images/<?php echo $reports["report_file"]; ?>"/> </td>
                    <td><?php echo date("d/m/Y", strtotime($reports['created_at']));?></td>
                    <td><?php echo date("d/m/Y", strtotime($reports['modified_at']));?></td>
                    <td>
                      <a href="view-single-report.php?id=<?php echo $reports['id'];?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="edit-report.php?id=<?php echo $reports['id'];?>" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="delete-report.php?id=<?php echo $reports['id'];?>" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>
                <?php }
                ?>
                
                
              </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>