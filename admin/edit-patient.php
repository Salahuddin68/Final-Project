<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `patients` WHERE id =".$_GET['id'];
// var_dump($query);
include 'header.php'; 
foreach($db->query($query) as $row) {
  $patients = $row;
}
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">      
            
           <a href="view-registered-patient-list.php" class="btn btn-info"><i class="fa fa-eye"></i> view all Patient</a>
            <div class="row">
                <div class="col-md-12" id="doctor-info-update">
                    
                        <h2>patients Account Setting:</h2>
                       <form action="update-patient.php" method="POST" enctype="multipart/form-data">
                       <input type="hidden" class="form-control" id="id" name="id" value="<?= $patients['id']; ?>">
                            <!-- <div class="form-group">
                                <label class="control-label col-sm-3" >ID:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="p_id" value="<?php echo $patients['id']; ?>" type="text" required="">
                                </div>
                            </div> -->
                           <div class="form-group">
                                <label class="control-label col-sm-3" >First Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_fname" value="<?php echo $patients['first_name']; ?>" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Last Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_lname" value="<?php echo $patients['last_name']; ?>"  type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Email:</label>
                                <div class="col-sm-9">
                                    <input class="form-control"  name="user_email" value="<?php echo $patients['email']; ?>" type="email" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Mobile:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_num" value="<?php echo $patients['contact_no']; ?>" placeholder="Enter Mobile" type="text">
                                </div>
                            </div>
                           <!--  <div class="form-group">
                                <label class="control-label col-sm-3">Address:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_address" value="<?php echo $patients['user_address']; ?>" placeholder="Enter Address" type="text">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Gender:</label>
                                <div class="col-sm-9">
                                    <select name="inlineRadioOptions" class="form-control" required="">
                                        <option value="Male"<?php if($patients['gender'] == 'Male'): ?> selected="selected"<?php endif; ?>>Male</option>
                                        <option value="Female"<?php if($patients['gender'] == 'Female'): ?> selected="selected"<?php endif; ?>>Female</option>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                
                                <label class="col-sm-3 control-label">Input image:</label>
                                <div class="col-sm-9">
                                    <img width="100px" style="margin-bottom: 15px;" src="images/<?php echo $patients["image"]; ?>"/>
                                    <input type="file" name="user_pic">
                                </div>
                                
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                       </form>
                  
                </div>
          </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>