<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `patients` WHERE id =".$_GET['id'];
// var_dump($query);
foreach($db->query($query) as $row) {
  $patients = $row;
}
include 'header.php'; 
?>

 

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">
        <a href="" class="btn btn-success"><i class="fa fa-plus"></i> Add New Patient</a>
        <a href="view-registered-patient-list.php" class="btn btn-info"><i class="fa fa-eye"></i> View All Patient</a>      
          

            <div class="row" style="margin-top: 40px;">
              <div class="col-md-4">
                <img  src="images/<?php echo $patients["image"]; ?>" alt="" class="img-thumbnail profile-image">
              </div>
              <div class="col-md-8">
                
                <div class="pro-desc">
                  <h3><strong><?php echo $patients['first_name'];?></strong></h3>

                  <table class="table table-striped">
                    <tbody>
                     <tr>
                      <td><strong>Fast Name</strong></td>
                      <td><?php echo $patients['first_name'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Last Name</strong></td>
                      <td><?php echo $patients['last_name'];?></a></td>
                    </tr>
                    <tr>
                      <td><strong>Email</strong></td>
                      <td><?php echo $patients['email'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Username</strong></td>
                      <td><?php echo $patients['username'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Gender</strong></td>
                      <td><?php echo $patients['gender'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Contact No:</strong></td>
                      <td><?php echo $patients['contact_no'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Created Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($patients['created_at']));?></td>
                    </tr>
                    <tr>
                      <td><strong>Modification Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($patients['modified_at']));?></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>

        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
