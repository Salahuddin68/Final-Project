<?php
// Start Session
session_start();
// check user login
if(empty($_SESSION['id']))
{
    header("Location: ../login.php");
}
// Database connection
require __DIR__ . '../../lib/connect.php';
$db = DB();
// Application library ( with DemoLib class )
require __DIR__ . '../../lib/library.php';
$app = new CmsAdminLib();
$user = $app->UserDetails($_SESSION['id']); // get user details

?>
<header>
	<div class="container">
		<div class="row">
		 
		<div class="logo inner-logo">
			<a href="../index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
		</div>
		 
		 
		<div class="tools-top">
		 
			<div class="avatar-profile">
				<div class="user-edit">
					<h4><strong><?php echo $user->first_name ;?> <?php echo $user->last_name ;?></strong></h4>
					<a href="my-account.php">edit profile</a>
				</div>
				<div class="avatar-image">

					<img width="38px" height="38px" src="../admin/images/<?php echo $user->image; ?>"/>
					<!-- <a href="appointments-reserved.php" title="2 Notifications Pending">
					<span class="notifications" style="display: inline;">2</span>
					</a> -->
				</div>
			</div>
			 
			<ul class="tools-help">
				<li><a href="p_help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
				<li><a href="../logout.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
			</ul>
		</div>
		 
		</div>
	</div>
</header>
 
<nav>
	<div class="container">
		<h4 class="navbar-brand">menu</h4>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<i class="fa fa-bars" aria-hidden="true"></i>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav main-menu">
				<li class="active"> <a href="my-account.php">my account</a> </li>
                <li> <a href="booked-calendar.php">book an appointment</a> </li>
                <li> <a href="appointments-reserved.php">appointments reserved</a> </li>
                <li> <a href="patients-examinations.php">Result Examinations</a> </li>
			</ul>
			
			</div>
		</div>
	</div>
</nav>

