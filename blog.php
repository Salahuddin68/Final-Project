<?php include 'header.php';  ?>

    <body>
        <header>
            <div class="menu">
                <div class="container">
                    <div class="row">
                        <div class="logo inner-logo">
                           <a href="index.php"><img src="images/logo-white.png" alt="logo main" class="img-responsive"></a>
                        </div>
                        <div class="meet-social">
                            <span><a href="#"><i class="fa fa-facebook-square"></i></a></span>
                            <span><a href="#"><i class="fa fa-twitter-square"></i></a></span>
                            <span><a href="#"><i class="fa fa-google-plus-square"></i></a></span>
                            <span><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--Menu-->
        <nav>
            <div class="container">
                <h4 class="navbar-brand">menu</h4>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li> <a href="index.php">Home</a> </li>
                        <li class="active"> <a href="blog.php">Blog</a> </li>
                        <li> <a href="login.php">Login</a> </li>
                        <!-- <li> <a href="my-account.php">my account</a> </li> -->
                        <li> <a href="help.php">Help </a></li>
                        <li> <a href="meet-doctors.php">Find Doctors</a> </li>
                        <!-- <li> <a href="404-page.php">Error Page - 404</a> </li> -->
                    </ul>
                </div>
            </div>
        </nav>
        <!--Menu-->

        <div class="blog">
            <div class="container">
                <div class="main-container">
                    <div class="row">

                        <div class="title-blog">
                            <h2>Our Blog</h2>
                            <hr>
                        </div>

                        <div class="list-blog">
                            <div class="row">
                                <!--Simple Img-->
                                <div class="item-blog">
                                    <div class="item-blog-content">
                                        <img src="images/welcome-login.jpg" alt="blog" class="img-responsive">
                                        <h4><a href="single-post.php">Who are our Doctors?</a></h4>
                                        <span class="entry-category"><a href="#">Reviews &amp; Opinion</a></span>
                                        <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                                        <p class="social-media">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                        </p>
                                        <p class="date">27 March 2017</p>
                                    </div>
                                </div>
                                <!--Simple Img-->


                                <!--Gallery-->
                                <div class="item-blog">
                                    <div class="item-blog-content">
                                        <ul class="gallery-list-post">
                                            <li><img src="images/book.jpg" alt="gallery post" class="img-responsive"></li>
                                            <li><img src="images/doc.jpg" alt="gallery post" class="img-responsive"></li>
                                            <li><img src="images/medication.jpg" alt="gallery post" class="img-responsive"></li>
                                            <li><img src="images/book.jpg" alt="gallery post" class="img-responsive"></li>
                                        </ul>
                                        <h4><a href="single-post.php">I'm a Gallery Post</a></h4>
                                        <span class="entry-category"><a href="#">Learn</a>, <a href="#">use</a></span>
                                        <p>This video is going to help you to learn how to learn our MEDICAL APPOINTMENT MANAGEMENT SYSTEM (IU)</p>
                                        <p class="social-media">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                        </p>
                                        <p class="date">27 March 2017</p>
                                    </div>
                                </div>
                                <!--Gallery-->

                                <!--Blockquote-->
                                <div class="item-blog">
                                    <div class="item-blog-content">
                                        <blockquote>
                                          <p><i class="fa fa-quote-left"></i> For quoting blocks of content from another source within your document. <i class="fa fa-quote-right"></i></p>
                                        </blockquote>
                                        <h4><a href="single-post.php">How to use our Platform?</a></h4>
                                        <span class="entry-category"><a href="#">Learn</a>, <a href="#">use</a></span>
                                        <p>This video is going to help you to learn how to learn our MEDICAL APPOINTMENT MANAGEMENT SYSTEM (IU)</p>
                                        <p class="social-media">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                        </p>
                                        <p class="date">27 March 2017</p>
                                    </div>
                                </div>
                                <!--Blockquote-->

                                <ul class="pager">
                                    <li class="previous"><a href="#"><span aria-hidden="true">←</span> Older</a></li>
                                    <li class="next"><a href="#">Newer <span aria-hidden="true">→</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <aside>
                            <h4>Categories</h4>
                            <hr>
                            <ul class="post_categories">
                                <li><a href="#">Reviews &amp; Opinion</a></li>
                                <li><a href="#">Lear</a></li>
                                <li><a href="#">Uses</a></li>
                                <li><a href="#">Documentation</a></li>
                            </ul>

                            <h4>Tags</h4>
                            <hr>
                            <ul class="post_tags">
                                <li><a href="#" title="tag title">Assessments</a></li>
                                <li><a href="#" title="tag title">Awards</a></li>
                                <li><a href="#" title="tag title">Budgeting</a></li>
                                <li><a href="#" title="tag title">Community</a></li>
                                <li><a href="#" title="tag title">Employees</a></li>
                                <li><a href="#" title="tag title">Paving</a></li>
                                <li><a href="#" title="tag title">Planning</a></li>
                                <li><a href="#" title="tag title">Potholes</a></li>
                                <li><a href="#" title="tag title">Proactive</a></li>
                            </ul>

                            <div class="elements-aside solid-color">
                                <ul>
                                    <li class="color-1">
                                        <i class="fa fa-heartbeat" aria-hidden="true"></i>
                                        <h4>Emergency Case</h4>
                                        <p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
                                    </li>
                                    <li class="color-3">
                                        <i class="fa fa-info" aria-hidden="true"></i>
                                        <h4>Doubts?</h4>
                                        <p>Office 8/2A, Katasur, Mohammadpur <span>Dhaka-1207</span></p>
                                    </li>
                                </ul>
                            </div>

                            <h4 class="login-aside">Login</h4>
                            <hr>
                            <form class="form-login">
                                <div class="icon-data">
                                    <i class="fa fa-address-card"></i>
                                </div>
                                <select>
                                    <option>Select a Document</option>
                                    <option>nit</option>
                                </select>

                                <div class="icon-data">
                                    <i class="fa fa-user-circle"></i>
                                </div>
                                <input type="number" placeholder="Number Document">

                                <div class="icon-data">
                                    <i class="fa fa-key"></i>
                                </div>
                                <input type="password" placeholder="Password">

                                <a href="appointments-reserved.php" class="btn btn-default btn-small" role="button">Login</a>
                                <!--<button class="btn btn-default" type="submit">Login</button>-->
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
        </div>

       <?php include 'footer.php';  ?>